package com.al1l.bukkit.gui;

import com.al1l.craftoblo.Craftoblo;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChestGui extends Gui<ChestGui> implements Listener {

    private int size = 0;
    private List<Player> playersOpen = new ArrayList<>();
    private Inventory inventory;
    private Inventory oldInventory;
    private boolean autoSize = false;
    private ItemStack filler = null;
    private List<Integer> filledSlots = new ArrayList<>();

    public static int pointToSlot(Point point) {
        int slot = (point.y * 9) + point.x;

        return slot;
        // 53 = (5*9)+x
    }

    public static Point slotToPoint(int s) {
        int y = (int) Math.floor(s / 9);
        int x = s - (y * 9);

        return new Point(x, y);
    }

    public static List<Integer> shapeToSlots(Rectangle shape) {
        List<Integer> points = new ArrayList<>();

        for (int x = shape.x; x < shape.width + shape.x; x++) {
            if (x > 8) {
                continue;
            }

            for (int y = shape.y; y < shape.height + shape.y; y++) {
                points.add(pointToSlot(new Point(x, y)));
                Collections.sort(points);
            }
        }

        return points;
    }

    public ItemStack getFiller() {
        return filler;
    }

    public void setFiller(ItemStack filler) {
        this.filler = filler;
    }

    public ChestGui build() {
        Inventory inv = Bukkit.createInventory(null, size, getTitle());

        for (GuiElement e : getGuiElements()) {
            e.onPlace(inv);
        }

        filledSlots = new ArrayList<>();

        for (GuiElement element : getGuiElements()) {
            filledSlots.addAll(shapeToSlots(element.getShape()));
        }

        if (filler != null) {
            for (int i = 0; i < size; i++) {
                if (!filledSlots.contains(i)) {
                    inv.setItem(i, filler);
                }
            }
        }

        this.oldInventory = this.inventory;
        this.inventory = inv;

        if (size != inv.getSize() && autoSize) {
            return build();
        }

        return this;
    }

    public int getSize() {
        return size;
    }

    public ChestGui setSize(int size) {
        this.size = size;

        if (isOpen()) {
            reopen();
        }

        return this;
    }

    public ChestGui setRows(int rows) {
        return setSize(rows * 9);
    }

    public Inventory getInventory() {
        return inventory;
    }

    public boolean isAutoSize() {
        return autoSize;
    }

    public void setAutoSize(boolean autoSize) {
        this.autoSize = autoSize;
    }

    public ChestGui reopen() {
        setRebuilding(true);

        if (isOpen()) {
            //for (Player p : players)
            //    if (p.getOpenInventory() != null)
            //        if (p.getOpenInventory().getTopInventory().equals(inventory))
            //            p.closeInventory();
            build();
            for (Player p : getPlayers()) {
                p.openInventory(inventory);
            }
        } else {
            build();

            for (Player p : getPlayers()) {
                p.openInventory(inventory);
            }

            setOpen(true);
        }

        setRebuilding(false);

        return this;
    }

    public ChestGui open(Player... ps) {
        if (isDisposed()) {
            return null;
        } else if (!isOpen()) {
            build();
        }

        for (Player p : ps) {
            if (p != null) {
                if (!getPlayers().contains(p)) {
                    getPlayers().add(p);
                }
                p.openInventory(inventory);
            }
        }

        setOpen(true);

        return this;
    }

    public ChestGui close(Player p) {
        if (getPlayers().contains(p)) {
            getPlayers().remove(p);

            if (p.getOpenInventory() != null) {
                if (p.getOpenInventory().getTopInventory().equals(inventory)) {
                    p.closeInventory();
                }
            }
        }

        if (getPlayers().size() == 0) {
            setOpen(false);
            if (isDisposeOnAllClose()) {
                this.dispose();
            }
        }

        return this;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getView() == null) {
            return;
        }

        if (e.getView().getTopInventory().equals(oldInventory)) {
            e.setCancelled(true);

            return;
        }

        if (e.getView().getTopInventory().equals(inventory)) {
            e.setCancelled(true);

            if (e.getClickedInventory() == null) {
                return;
            }

            if (e.getClickedInventory().equals(inventory)) {
                for (GuiElement elmt : getGuiElements()) {
                    List<Integer> slots = shapeToSlots(elmt.getShape());

                    if (slots.contains(e.getSlot())) {
                        if (!elmt.isReadOnly()) {
                            e.setCancelled(false);
                        }

                        elmt.onClick(e);

                        return;
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onInventoryClose(InventoryCloseEvent e) {
        if (e.getView() != null) {
            if (e.getView().getTopInventory() != null) {
                if (e.getView().getTopInventory().equals(inventory)) {
                    if (!isRebuilding()) {
                        if (e.getPlayer() != null) {
                            if (getOnCloseEvent() != null) {
                                getOnCloseEvent().on(this, (Player) e.getPlayer());
                            }
                        }

                        getPlayers().remove(e.getPlayer());
                    }
                    if (getPlayers().size() == 0 && isDisposeOnAllClose()) {
                        this.dispose();
                    }
                }
            }
        }

    /*
    0  1  2  3  4  5  6  7  8
    9  10 11 12 13 14 15 16 17
    18 19 20 21 22 23 24 25 26
    27 28 29 30 31 32 33 34 35
    36 37 38 39 40 41 42 43 44
    45 46 47 48 49 50 51 52 53

    1  2  3  4  5  6  7  8  9
    10 11 12 13 14 15 16 17 18
    19 20 21 22 23 24 25 26 27
    28 29 30 31 32 33 34 35 36
    37 38 39 40 41 42 43 44 45
    46 47 48 49 50 51 52 53 54
     */
    }
}