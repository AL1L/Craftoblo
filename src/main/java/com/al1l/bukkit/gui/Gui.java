package com.al1l.bukkit.gui;

import com.al1l.craftoblo.Craftoblo;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public abstract class Gui<T extends Gui> implements Listener {

    private List<GuiElement> guiElements = new ArrayList<>();
    private String title = "GUI";
    @Getter
    private List<Player> players = new ArrayList<>();
    private List<Player> playersOpen = new ArrayList<>();
    @Getter
    @Setter
    private boolean isOpen = false;
    @Getter
    private GuiEvent onCloseEvent;
    @Getter
    @Setter
    private boolean isRebuilding = false;
    @Getter
    private boolean disposeOnAllClose = true;
    @Getter
    private boolean disposed = false;

    public Gui() {
        Bukkit.getPluginManager().registerEvents(this, Craftoblo.gi());
    }
    public static List<Point> shapeToPoints(Rectangle shape) {
        List<Point> points = new ArrayList<>();

        for (int x = shape.x; x < shape.width + shape.x; x++) {
            if (x > 8) {
                continue;
            }

            for (int y = shape.y; y < shape.height + shape.height; y++) {
                points.add(new Point(x, y));
            }
        }

        return points;
    }

    public static Rectangle getShape(Point p1, Point p2) {
        Rectangle shape = new Rectangle();
        shape.setFrameFromDiagonal(p1, p2);

        return shape;
    }

    public void setRebuilding(boolean rebuilding) {
        this.isRebuilding = rebuilding;
    }

    public T onClose(GuiEvent event) {
        onCloseEvent = event;

        return (T) this;
    }

    public T add(GuiElement element) {
        guiElements.add(element);
        element.setGui((T) this);

        return (T) this;
    }

    public T addAll(List<GuiElement> elements) {
        guiElements.addAll(elements);

        for (GuiElement e : elements) {
            e.setGui((T) this);
        }

        return (T) this;
    }

    public T remove(GuiElement element) {
        guiElements.remove(element);
        element.setGui(null);

        return (T) this;
    }

    public T removeAll(List<GuiElement> elements) {
        guiElements.removeAll(elements);

        for (GuiElement e : elements) {
            e.setGui(null);
        }

        return (T) this;
    }

    public abstract T build();

    public String getTitle() {
        return title;
    }

    public T setTitle(String title) {
        this.title = title;

        if (isOpen) {
            reopen();
        }

        return (T) this;
    }

    public abstract T reopen();

    public abstract T open(Player... ps);

    public abstract T close(Player p);

    /**
     * Close all, unregister event listener, and null all values.
     */
    void dispose() {
        if (disposed) {
            return;
        }
        this.disposed = true;
        this.closeAll();
        HandlerList.unregisterAll(this);
        this.guiElements = null;
        this.title = null;
        this.players = null;
        this.isOpen = false;
        this.onCloseEvent = null;
        this.isRebuilding = false;
        this.disposeOnAllClose = true;
    }

    public T closeAll() {
        List<Player> ps = new ArrayList<>();

        ps.addAll(this.players);

        for (Player p : ps) {
            close(p);
        }

        isOpen = false;

        return (T) this;
    }

    public List<GuiElement> getGuiElements() {
        List<GuiElement> elements = new ArrayList<>();
        elements.addAll(guiElements);

        return elements;
    }

    public void setDisposeOnAllClose(boolean disposeOnAllClose) {
        this.disposeOnAllClose = disposeOnAllClose;
    }
}