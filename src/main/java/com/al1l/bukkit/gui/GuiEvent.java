package com.al1l.bukkit.gui;

import org.bukkit.entity.Player;

@FunctionalInterface
public interface GuiEvent {
    public void on(ChestGui gui, Player player);
}
