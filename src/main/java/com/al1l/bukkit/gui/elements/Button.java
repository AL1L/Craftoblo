package com.al1l.bukkit.gui.elements;

import com.al1l.bukkit.gui.ChestGui;
import com.al1l.bukkit.gui.GuiElement;
import com.al1l.bukkit.gui.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.awt.*;
import java.util.List;


public abstract class Button extends GuiElement<Button, ChestGui> implements Resizeable {
    private ItemStack item;

    public Button() {
        this.item = new ItemBuilder(Material.BARRIER).displayName(" ").build();
    }

    public Button(ItemStack item) {
        this.item = item;
    }

    public Button(ItemBuilder item) {
        this.item = item.build();
    }

    public ItemStack getItem() {
        return item;
    }

    public Button setItem(ItemStack item) {
        this.item = item;
        return this;
    }

    @Override
    public Button setSize(int height, int width) {
        Rectangle shape = getShape();
        shape.width = width;
        shape.height = height;
        setShape(shape);
        return this;
    }

    @Override
    public void onPlace(Inventory i) {
        if (item == null) {
            return;
        }

        if (isAutoPlace()) {
            setPosistion(ChestGui.slotToPoint(i.firstEmpty()), false);
        }

        if (getGui().isAutoSize()) {
            if (ChestGui.slotToPoint(getGui().getSize() - 1).y < getShape().y || getGui().getSize() == 0) {
                getGui().setSize(ChestGui.pointToSlot(new Point(8, getShape().y)) + 1);
                return;
            }
        }

        List<Integer> slots = ChestGui.shapeToSlots(getShape());

        for (Integer s : slots) {
            if (s < getGui().getSize()) {
                i.setItem(s, item);
            }
        }
    }
}
