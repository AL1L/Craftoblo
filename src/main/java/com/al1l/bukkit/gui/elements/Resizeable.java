package com.al1l.bukkit.gui.elements;


import com.al1l.bukkit.gui.GuiElement;

public interface Resizeable<T extends GuiElement> {
    T setSize(int height, int width);
}
