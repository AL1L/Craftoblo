package com.al1l.bukkit.gui;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.awt.*;

public abstract class GuiElement<T extends GuiElement, G extends Gui> {

    private Rectangle shape;
    private boolean readOnly = true;
    private G gui = null;
    private boolean autoPlace = true;

    public GuiElement(Rectangle shape) {
        this.shape = shape;
    }

    public GuiElement() {
        shape = new Rectangle(1, 1);
    }

    public G getGui() {
        return gui;
    }

    void setGui(G gui) {
        this.gui = gui;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public int getHeight() {
        return shape.height;
    }

    public int getWidth() {
        return shape.width;
    }

    public int getX() {
        return shape.x;
    }

    public int getY() {
        return shape.y;
    }

    public T setPosistion(int x, int y, boolean disableAutoPlace) {
        shape.x = x;
        shape.y = y;
        if (disableAutoPlace)
            autoPlace = false;
        return (T) this;
    }

    public T setPosistion(int x, int y) {
        return setPosistion(x, y, true);
    }

    public T setPosistion(Point p) {
        return setPosistion(p.x, p.y, true);
    }

    public T setPosistion(Point p, boolean disableAutoPlace) {
        return setPosistion(p.x, p.y, disableAutoPlace);
    }

    public boolean isAutoPlace() {
        return autoPlace;
    }

    public T setAutoPlace(boolean autoPlace) {
        this.autoPlace = autoPlace;
        return (T) this;
    }

    public Rectangle getShape() {
        return shape;
    }

    public void setShape(Rectangle shape) {
        this.shape = shape;
    }

    public abstract void onPlace(Inventory i);

    public abstract void onClick(InventoryClickEvent e);
}
