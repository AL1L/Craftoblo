package com.al1l.craftoblo;

import com.al1l.bukkit.util.FileLoader;
import lombok.Getter;
import lombok.Setter;
import com.al1l.craftoblo.animation.AnimationManager;
import com.al1l.craftoblo.camera.CameraManager;
import com.al1l.craftoblo.chat.Formatter;
import com.al1l.craftoblo.items.ItemManager;
import com.al1l.craftoblo.playerdata.DataManager;
import com.al1l.craftoblo.playerdata.User;
import com.al1l.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class Craftoblo extends JavaPlugin {

    @Getter
    private final AnimationManager animationManager;
    @Getter
    @Setter
    private ItemManager itemManager;
    @Getter
    private Formatter formatter = new Formatter(this);

    public Craftoblo() {
        this.animationManager = new AnimationManager();
    }

    public static Craftoblo gi() {
        return (Craftoblo) Bukkit.getPluginManager().getPlugin("Craftoblo");

    }

    public static ItemStack changeLoreLine(ItemStack stack, int line, String newValue) {
        ItemMeta meta = stack.getItemMeta();
        List<String> lore = meta.getLore();
        while (lore.size() < line)
            lore.add(" ");
        lore.set(line, newValue);
        meta.setLore(lore);
        stack.setItemMeta(meta);
        return stack;
    }

    @Override
    public void onEnable() {
        MainConfigManager.checkConfig();
        FileLoader.checkFiles();
        Registers.registerAll();
        DataManager.afterReload();
        for (User u : DataManager.getAllOnlineUsers()) {
            u.sendNotification("§b§oA new §a§oCraftoblo §b§oupdate has been completed!");
        }
        getLogger().info("Craftoblo fully enabled.");
    }

    @Override
    public void onDisable() {
        for (User user : DataManager.getAllOnlineUsers()) {
            CameraManager.getManager().detach(user.getPlayer());
            SetupScorboard.bossBars.get(user.getPlayer().getUniqueId()).setVisible(false);
            user.save();
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.RED + "This command is broken, please contact an administrator about this problem."
                + ChatColor.GRAY + " (ER: 0001)");
        return true;
    }

}
