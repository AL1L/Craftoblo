package com.al1l.craftoblo.actions;

import com.al1l.craftoblo.Craftoblo;
import com.al1l.craftoblo.abilitys.AbilityObject;
import com.al1l.craftoblo.camera.CameraManager;
import com.al1l.craftoblo.playerdata.DataManager;
import com.al1l.craftoblo.playerdata.User;
import com.al1l.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class LevelUp {

    public static void play(final Player p, final boolean changeLevel) {
        if (!CameraManager.getManager().isAttatched(p))
            return;
        User user = DataManager.getUser(p);
        final double level = user.getLevel();
        final int nextLevel = (int) Math.floor(level + 1);
        new BukkitRunnable() {
            int times = 0;

            public void run() {
                if (times > 100) {
                    this.cancel();
                    return;
                }
                Location loc = CameraManager.getManager().getArmorStand(p).getEyeLocation().clone().add(0, 0.3, 0);
                loc.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, loc, 20);
                times++;
                ChatColor color;
                switch (String.valueOf((double) times / 2).split("\\.")[1]) {
                    case "0":
                        color = ChatColor.YELLOW;
                        break;
                    case "5":
                        color = ChatColor.GOLD;
                        break;
                    default:
                        color = ChatColor.WHITE;
                        break;
                }
                if (changeLevel) {
                    p.sendTitle(color + "You have reached",
                            ChatColor.WHITE + "" + ChatColor.BOLD + "Level " + nextLevel, 0, 2, 80);
                } else {
                    p.sendTitle(color + "You have reached",
                            ChatColor.WHITE + "" + ChatColor.BOLD + "Level " + ((int) Math.floor(level)), 0, 10, 80);
                }
                // ActionBar.sendAction(p, "&b&k|&5 Level Up! &b&k|", true);
            }
        }.runTaskTimer(Craftoblo.gi(), 0L, 1L);
        if (changeLevel) {
            user.setLevel(nextLevel);

            SetupScorboard.set(p);

            // inv setup
            PlayerInventory inv = p.getInventory();
            inv.clear();
            inv.setHeldItemSlot(7);
            List<AbilityObject> abilityObject = user.getEquippedAbilities();
            for (int i = 0; i < abilityObject.size(); i++)
                user.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(user.getPlayer()));
        }
    }
}
