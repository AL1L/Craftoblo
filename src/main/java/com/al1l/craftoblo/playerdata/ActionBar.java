package com.al1l.craftoblo.playerdata;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ActionBar {

    public static void sendAction(Player p, String msg, boolean translateColors) {
        if (translateColors) {
            msg = ChatColor.translateAlternateColorCodes('&', msg);
        }
        p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(msg));
    }
}