package com.al1l.craftoblo.playerdata;

import com.al1l.craftoblo.Craftoblo;
import com.al1l.craftoblo.Log;
import com.al1l.craftoblo.abilitys.AbilityManager;
import com.al1l.craftoblo.abilitys.AbilityObject;
import com.al1l.craftoblo.camera.CameraManager;
import com.al1l.craftoblo.classes.ClassType;
import com.al1l.craftoblo.scoreboard.SetupScorboard;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class DataManager implements Listener {
    @Getter
    private static List<User> allOnlineUsers = new ArrayList<>();

    public static User getUser(Player player) {
        for (User user : allOnlineUsers)
            if (user.getPlayer().getUniqueId().equals(player.getUniqueId()))
                return user;
        return null;
    }

    public static void afterReload() {
        allOnlineUsers = new ArrayList<>();
        for (final Player player : Bukkit.getOnlinePlayers()) {
            User user;
            try {
                user = new User(player);
            } catch (Exception e) {
                for (Player p : Bukkit.getOnlinePlayers())
                    if (p.isOp()) {
                        p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.DARK_RED + player.getName() + " TRIED TO JOIN BUT THERE WAS AN ERROR GETTING THEIR DATA!");
                        Bukkit.getServer().getLogger().log(Level.SEVERE, player.getName() + " TRIED TO JOIN BUT THERE WAS AN ERROR GETTING THEIR DATA!");
                    }
                player.kickPlayer(ChatColor.AQUA + "There was an error getting your information!\n" + ChatColor.AQUA + "Please email {SUPPORT_EMAIL} for more help.");
                e.printStackTrace();
                return;
            }
            allOnlineUsers.add(user);

            AbilityManager.setPlayersAbilitys(player);
            try {
                CameraManager.getManager().attach(player, 5, 5, 5);
            } catch (Exception e) {
                Log.warning("Error when attaching " + player.getName() + "!");
                e.printStackTrace();
            }

            new BukkitRunnable() {

                public void run() {
                    if (!player.isOnline())
                        this.cancel();
                    try {
                        SetupScorboard.set(player);
                    } catch (Exception e) {
                        Log.warning("Error when setting " + player.getName() + "'s scoreboard!");
                        e.printStackTrace();
                    }

                    if (!player.isOnline())
                        this.cancel();

                }
            }.runTaskTimer(Craftoblo.gi(), 0, 25);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {
        User user;
        try {
            user = new User(event.getPlayer());
        } catch (Exception e) {
            Bukkit.getServer().getLogger().log(Level.SEVERE, event.getPlayer().getName() + " TRIED TO JOIN BUT THERE WAS AN ERROR GETTING THEIR DATA!");
            for (Player player : Bukkit.getOnlinePlayers())
                if (player.isOp()) {
                    player.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.DARK_RED + event.getPlayer().getName() + " TRIED TO JOIN BUT THERE WAS AN ERROR GETTING THEIR DATA!");
                }
            event.getPlayer().kickPlayer(ChatColor.AQUA + "There was an error getting your information!\n" + ChatColor.AQUA + "Please email {SUPPORT_EMAIL} for more help.");
            e.printStackTrace();
            return;
        }
        Player p = user.getPlayer();
        allOnlineUsers.add(user);

        List<AbilityObject> abilityObject = user.getEquippedAbilities();
        for (int i = 0; i < abilityObject.size(); i++)
            user.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(user.getPlayer()));

        new BukkitRunnable() {

            public void run() {
                if (!p.isOnline())
                    this.cancel();
                SetupScorboard.set(user.getPlayer());
            }
        }.runTaskTimer(Craftoblo.gi(), 0, 25);
        if (!p.hasPlayedBefore() || user.getConfig().getString("CLASS", "NONE").equals("NONE")) {
            event.setJoinMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "(!) " + ChatColor.WHITE + "The new comer " + p.getName() + ", has joined the game!");
            user.getPlayer().sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!) " + ChatColor.YELLOW + "Do \"/class\" to get started!");
        } else {
            event.setJoinMessage(ChatColor.GREEN + "" + ChatColor.BOLD + "(!) "
                    + user.getCurrentClass().getColor() + "The " + user.getCurrentClass().getName() + ", " + p.getName() + ", has joined the game!");
        }
        String msgPredix = ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.YELLOW + " ";
        p.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "+---------------------------------------------------+");
        p.sendMessage(Craftoblo.gi().getFormatter().centerString(ChatColor.YELLOW + "This server REQUIRES a fast internet connection!" +
                " If you have a slow connection, then you will not get the full experience of the server. This server is also in ALPHA phase and there is not much to do."));
        p.sendMessage(ChatColor.GRAY + "" + ChatColor.STRIKETHROUGH + "+---------------------------------------------------+");


        p.sendMessage(msgPredix + "Go to §9§nhttp://www.craftoblo.ml/index.php/tutorial§e to learn about §lCraftoblo§e and how to use it.");
        p.sendTitle(ChatColor.GOLD + "" + ChatColor.BOLD + "Welcome!",
                ChatColor.RED + "" + ChatColor.BOLD + "Check chat for important info! ", 0, 20 * 3, 80);
    }


    @EventHandler(priority = EventPriority.LOWEST)
    public void onLeave(PlayerQuitEvent event) {
        User user = getUser(event.getPlayer());
        user.save();
        if (user.getCurrentClass().equals(ClassType.NONE)) {
            event.setQuitMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!) " + ChatColor.WHITE + "The new comer " + user.getPlayer().getName() + ", has left the game!");
        } else {
            event.setQuitMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!) "
                    + user.getCurrentClass().getColor() + "The " + user.getCurrentClass().getName() + ", " + user.getPlayer().getName() + ", has left the game!");
        }

        allOnlineUsers.remove(user);
    }

}
