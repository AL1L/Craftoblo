package com.al1l.craftoblo;

import java.util.HashMap;
import java.util.UUID;

public class Variables {

    public static final int movingTimeout = 5; // Ticks
    public static final HashMap<UUID, Integer> moving = new HashMap<>();
}
