package com.al1l.craftoblo.commands;

import com.al1l.craftoblo.abilitys.AbilityObject;
import com.al1l.craftoblo.camera.CameraManager;
import com.al1l.craftoblo.playerdata.DataManager;
import com.al1l.craftoblo.playerdata.User;
import com.al1l.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class FixCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You have nothing to be fixed");
            return true;
        }
        Player p = (Player) sender;
        PlayerInventory inv = p.getInventory();
        User u = DataManager.getUser(p);
        List<AbilityObject> abilityObject = u.getEquippedAbilities();

        if (args.length == 0 || args[0].equalsIgnoreCase("all") || args[0].equalsIgnoreCase("a")) {
            u.sendMessage("Fixing player.");
            // Camera detach
            CameraManager.getManager().detach(p);

            // Reposition if needed
            if (!p.getLocation().getBlock().getType().equals(Material.AIR)
                    || !p.getEyeLocation().getBlock().getType().equals(Material.AIR)) {
                u.sendMessage("Invalid location! Sending you to spawn.");
                p.teleport(p.getLocation().getWorld().getSpawnLocation().getBlock().getLocation().add(0.5, 0, 0.5));
            }

            // inv setup
            inv.clear();
            inv.setHeldItemSlot(7);
            for (int i = 0; i < abilityObject.size(); i++)
                u.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(u.getPlayer()));

            // Scorboard reset
            SetupScorboard.set(p);

            // Camera attach
            CameraManager.getManager().attach(p, 5, 5, 5);
        } else if (args[0].equalsIgnoreCase("location") || args[0].equalsIgnoreCase("l")) {
            u.sendMessage("Fixing location.");
            CameraManager.getManager().detach(p);
            p.teleport(p.getLocation().getWorld().getSpawnLocation().getBlock().getLocation().add(0.5, 0, 0.5));
            CameraManager.getManager().attach(p, 5, 5, 5);
        } else if (args[0].equalsIgnoreCase("abilitys") || args[0].equalsIgnoreCase("items")
                || args[0].equalsIgnoreCase("ab") || args[0].equalsIgnoreCase("i")) {
            u.sendMessage("Fixing items/abilitys.");
            inv.clear();
            inv.setHeldItemSlot(7);
            for (int i = 0; i < abilityObject.size(); i++)
                u.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(u.getPlayer()));

        } else if (args[0].equalsIgnoreCase("scorboard") || args[0].equalsIgnoreCase("s")) {
            u.sendMessage("Fixing scoreboard.");
            SetupScorboard.set(p);

        } else {
            u.sendError("Unknown Syntax");
        }

        return true;
    }
}
