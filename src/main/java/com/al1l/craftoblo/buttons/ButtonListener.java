package com.al1l.craftoblo.buttons;

public abstract class ButtonListener {

    public abstract void buttonPressedEvent(ButtonPressEvent e);

}
