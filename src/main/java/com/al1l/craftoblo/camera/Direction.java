package com.al1l.craftoblo.camera;

  public enum Direction {
      NORTH(180), NORTH_EAST(225), EAST(270), SOUTH_EAST(315), SOUTH(0), SOUTH_WEST(45), WEST(90), NORTH_WEST(135);

      private final int degree;

      Direction(int degree) {
          this.degree = degree;
      }

      public int toDegree() {
          return this.degree;
      }

      public static double normalizeDegree(double d) {
          d = ((d / 360) - Math.floor(d / 360)) * 360;
          if (d < 0) {
              d = 360 + d;
          }
          return d;
      }

      public static Direction fromDegree(double d) {
          d = normalizeDegree(d);
          if (d <= 22.5 && d > 337.5) {
              return SOUTH;
          } else if (d <= 67.5 && d > 22.5) {
              return SOUTH_WEST;
          } else if (d <= 112.5 && d > 67.5) {
              return WEST;
          } else if (d <= 157.5 && d > 112.5) {
              return NORTH_WEST;
          } else if (d <= 202.5 && d > 157.5) {
              return NORTH;
          } else if (d <= 247.5 && d > 202.5) {
              return NORTH_EAST;
          } else if (d <= 292.5 && d > 247.5) {
              return EAST;
          } else if (d <= 337.5 && d > 292.5) {
              return SOUTH_EAST;
          }
          return SOUTH;
      }
  }