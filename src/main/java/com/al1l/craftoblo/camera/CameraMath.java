package com.al1l.craftoblo.camera;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

class CameraMath {
    public static Direction getMovingDirection(Location from, Location to) {
        double angle = Math.toDegrees(Math.atan2(to.getX() - from.getX(), to.getZ() - from.getZ()));
        return Direction.fromDegree(angle);
    }

    public static float[] faceLocation(Entity toFace, Location loc) {
        Location entityLoc = toFace.getLocation();
        if (toFace instanceof LivingEntity) {
            entityLoc = ((LivingEntity) toFace).getEyeLocation();
        }
        double diffX = loc.getX() - entityLoc.getX();
        double diffY = loc.getY() - entityLoc.getY();
        double diffZ = loc.getZ() - entityLoc.getZ();
        double dist = Math.sqrt(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / Math.PI);
        return new float[]{entityLoc.getYaw() + wrapAngleTo180(yaw - entityLoc.getYaw()), entityLoc.getPitch() + wrapAngleTo180(pitch - entityLoc.getPitch())};
    }

    public static float wrapAngleTo180(float value) {
        value = value % 360.0F;
        if (value >= 180.0F)
            value -= 360.0F;
        if (value < -180.0F)
            value += 360.0F;
        return value;
    }
}
