package com.al1l.craftoblo.camera;

import com.al1l.craftoblo.Craftoblo;
import com.al1l.craftoblo.menus.ClassSelection;
import com.al1l.craftoblo.menus.SkillsMenu;
import com.al1l.craftoblo.animation.Animation;
import com.al1l.craftoblo.playerdata.DataManager;
import com.al1l.craftoblo.playerdata.User;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.List;

public class CameraListener implements Listener {
    private final boolean debug = Craftoblo.gi().getConfig().getBoolean("Options.Debug");
    private final Material[] allowedBlocks = {Material.AIR, Material.TORCH, Material.RAILS, Material.ACTIVATOR_RAIL,
            Material.DETECTOR_RAIL, Material.RAILS, Material.BROWN_MUSHROOM, Material.RED_MUSHROOM, Material.LONG_GRASS,
            Material.SAPLING, Material.YELLOW_FLOWER, Material.CHORUS_FLOWER, Material.FLOWER_POT, Material.SNOW,
            Material.LADDER, Material.VINE, Material.DOUBLE_PLANT, Material.END_ROD, Material.SKULL, Material.BANNER,
            Material.STANDING_BANNER, Material.WALL_BANNER, Material.SIGN, Material.CARPET, Material.WHEAT,
            Material.STONE_BUTTON, Material.WOOD_BUTTON, Material.CARROT, Material.POTATO, Material.BEETROOT_BLOCK,
            Material.SUGAR_CANE_BLOCK, Material.DEAD_BUSH, Material.WEB, Material.REDSTONE_WIRE, Material.LEVER,
            Material.STONE_PLATE, Material.WOOD_PLATE, Material.GOLD_PLATE, Material.IRON_PLATE, Material.WATER,
            Material.STATIONARY_WATER};
    private final List<Material> allowedBlocksList = Arrays.asList(allowedBlocks);
    private final Animation walkAnimation;

    public CameraListener() {
        walkAnimation = Craftoblo.gi().getAnimationManager().getAnimation("DEFAULT_WALK");
    }


    @EventHandler(priority = EventPriority.MONITOR)
    public void onLeave(PlayerQuitEvent e) {
        if (CameraManager.getManager().isAttatched(e.getPlayer()))
            CameraManager.getManager().detach(e.getPlayer());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onJoin(final PlayerJoinEvent e) {
        Location spawnLoc = e.getPlayer().getWorld().getSpawnLocation().add(0.5, 0, 0.5);
        e.getPlayer().teleport(spawnLoc);
        new BukkitRunnable() {

            public void run() {
                CameraManager.getManager().attach(e.getPlayer(), 5, 5, 5);

            }
        }.runTaskLater(Craftoblo.gi(), 30);

        User user = DataManager.getUser(e.getPlayer());

        if (!e.getPlayer().hasPlayedBefore() || user.getConfig().getString("CLASS", "NONE").equals("NONE")) {
            ClassSelection menu = new ClassSelection();
            new BukkitRunnable() {

                public void run() {
                    menu.open(e.getPlayer());
                }
            }.runTaskLater(Craftoblo.gi(), 10);
        }
    }

    @EventHandler
    public void creativeInvClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory().getType().equals(InventoryType.PLAYER) && CameraManager.getManager().isAttatched(p)) {
            e.setCancelled(true);

        }
    }

    @EventHandler
    public void runeClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory().getType().equals(InventoryType.PLAYER) && CameraManager.getManager().isAttatched(p))
            if (e.getSlot() >= 9 && e.getSlot() <= 35)
                p.sendMessage(ChatColor.AQUA + "The runes menu has not yet been created. (" + e.getSlot() + ")");

    }

    @EventHandler
    public void invClock(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (CameraManager.getManager().isAttatched(p))
            if (e.getClickedInventory().getType().equals(InventoryType.PLAYER)) {
                if (e.getSlot() >= 36 && e.getSlot() <= 39)
                    p.sendMessage(ChatColor.AQUA + "The inventory menu has not yet been created. (" + e.getSlot() + ")");
            } else if (e.getClickedInventory().getType().equals(InventoryType.CRAFTING)) {
                p.sendMessage(ChatColor.AQUA + "The inventory menu has not yet been created. (" + e.getSlot() + ")");
            }

    }

    @EventHandler
    public void abilityClick(InventoryClickEvent e) {
        if (e.getClickedInventory() == null)
            return;
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory().getType().equals(InventoryType.PLAYER) && CameraManager.getManager().isAttatched(p))
            if (e.getSlot() >= 0 && e.getSlot() <= 5)
                new SkillsMenu().open(p);

    }

    @EventHandler
    public void playerDrop(PlayerDropItemEvent e) {
        if (CameraManager.getManager().isAttatched(e.getPlayer())) {
            e.setCancelled(true);
        }

    }

    @EventHandler
    public void onInvOpen(InventoryOpenEvent e) {
        if (CameraManager.getManager().isAttatched((Player) e.getPlayer())) {
            if (e.getInventory() != null && e.getInventory().getType() == InventoryType.PLAYER) {
                e.setCancelled(true);
            }
        }

    }

}
