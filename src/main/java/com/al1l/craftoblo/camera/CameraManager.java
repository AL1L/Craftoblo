package com.al1l.craftoblo.camera;

import com.al1l.craftoblo.Craftoblo;
import lombok.Getter;
import com.al1l.craftoblo.animation.Animation;
import com.al1l.craftoblo.playerdata.User;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class CameraManager {
    static final boolean debug = Craftoblo.gi().getConfig().getBoolean("Options.Debug");
    private static final Material[] allowedBlocks = {Material.AIR, Material.TORCH, Material.RAILS, Material.ACTIVATOR_RAIL,
            Material.DETECTOR_RAIL, Material.RAILS, Material.BROWN_MUSHROOM, Material.RED_MUSHROOM, Material.LONG_GRASS,
            Material.SAPLING, Material.YELLOW_FLOWER, Material.CHORUS_FLOWER, Material.FLOWER_POT, Material.SNOW,
            Material.LADDER, Material.VINE, Material.DOUBLE_PLANT, Material.END_ROD, Material.SKULL, Material.BANNER,
            Material.STANDING_BANNER, Material.WALL_BANNER, Material.SIGN, Material.CARPET, Material.WHEAT,
            Material.STONE_BUTTON, Material.WOOD_BUTTON, Material.CARROT, Material.POTATO, Material.BEETROOT_BLOCK,
            Material.SUGAR_CANE_BLOCK, Material.DEAD_BUSH, Material.WEB, Material.REDSTONE_WIRE, Material.LEVER,
            Material.STONE_PLATE, Material.WOOD_PLATE, Material.GOLD_PLATE, Material.IRON_PLATE, Material.WATER,
            Material.STATIONARY_WATER};
    static final List<Material> allowedBlocksList = Arrays.asList(allowedBlocks);
    static Animation walkAnimation;
    private static CameraManager mainManager = null;
    @Getter
    private final HashMap<UUID, Camera> cameras = new HashMap<>();

    public static CameraManager getManager() {
        if (mainManager == null)
            mainManager = new CameraManager();
        return mainManager;
    }

    public CameraManager() {
        this.walkAnimation = Craftoblo.gi().getAnimationManager().getAnimation("DEFAULT_WALK");
    }

    public void attach(Player p, double xOffest, double yOffest, double zOffest) {
        if (!cameras.containsKey(p.getUniqueId()))
            cameras.put(p.getUniqueId(), new Camera(Craftoblo.gi(), p));

        Camera cam = cameras.get(p.getUniqueId());
        cam.setOffset(xOffest, yOffest, zOffest);
        cam.attach();

        ArmorStand stand = cam.getStand();
        User user = cam.getUser();

        String rank = "";
        if (p.isOp())
            rank = ChatColor.DARK_PURPLE + "*";
        try {
            stand.setCustomName(rank + ChatColor.RESET
                    + user.getCurrentClass().getColor() + p.getDisplayName() + " ("
                    + user.getCurrentClass().getName() + ")");
        } catch (Exception e) {
            stand.setCustomName(ChatColor.RED + p.getDisplayName() + " (ERROR)");
            user.sendError("There was an error when getting your data, please contact an administrator about this problem.");
        }
        p.setPlayerListName(rank + ChatColor.RESET
                + user.getCurrentClass().getColor()
                + p.getDisplayName());
        stand.setCustomNameVisible(true);
        ItemStack skull = new ItemStack(Material.SKULL_ITEM);
        skull.setDurability((short) 3);
        SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.setOwner(p.getName());
        skull.setItemMeta(sm);
        stand.setHelmet(skull);
    }

    public Float[] faceLocation(Entity toFace, Location loc) {
        Location entityLoc = toFace.getLocation();
        if (toFace instanceof LivingEntity) {
            entityLoc = ((LivingEntity) toFace).getEyeLocation();
        }
        double diffX = loc.getX() - entityLoc.getX();
        double diffY = loc.getY() - entityLoc.getY();
        double diffZ = loc.getZ() - entityLoc.getZ();
        double dist = Math.sqrt(diffX * diffX + diffZ * diffZ);
        float yaw = (float) (Math.atan2(diffZ, diffX) * 180.0D / Math.PI) - 90.0F;
        float pitch = (float) -(Math.atan2(diffY, dist) * 180.0D / Math.PI);
        return new Float[]{entityLoc.getYaw() + wrapAngleTo180(yaw - entityLoc.getYaw()), entityLoc.getPitch() + wrapAngleTo180(pitch - entityLoc.getPitch())};
    }

    public float wrapAngleTo180(float value) {
        value = value % 360.0F;
        if (value >= 180.0F)
            value -= 360.0F;
        if (value < -180.0F)
            value += 360.0F;
        return value;
    }

    public void detach(Player p) {
        if (!cameras.containsKey(p.getUniqueId()))
            return;

        cameras.get(p.getUniqueId()).detach();
    }

    public void moveArmorStand(Player p, Location loc) {
        if (!cameras.containsKey(p.getUniqueId()))
            return;

        cameras.get(p.getUniqueId()).moveStand(loc);

    }

    public boolean isAttatched(Player p) {
        if (!cameras.containsKey(p.getUniqueId()))
            return false;

        return cameras.get(p.getUniqueId()).isAttached();
    }

    public ArmorStand getArmorStand(Player p) {
        if (!cameras.containsKey(p.getUniqueId()))
            return null;

        return cameras.get(p.getUniqueId()).getStand();
    }


}