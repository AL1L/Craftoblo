package com.al1l.craftoblo.camera;

import lombok.Getter;
import lombok.Setter;
import com.al1l.craftoblo.Craftoblo;
import com.al1l.craftoblo.Variables;
import com.al1l.craftoblo.playerdata.ActionBar;
import com.al1l.craftoblo.playerdata.DataManager;
import com.al1l.craftoblo.playerdata.User;
import com.al1l.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Camera implements Listener {

    @Getter
    private String title;
    @Getter
    @Setter
    private double xOffset;
    @Getter
    @Setter
    private double yOffset;
    @Getter
    @Setter
    private double zOffset;
    @Getter
    private float[] cachedAngles;
    @Getter
    private String message;
    @Getter
    private boolean attached;
    @Getter
    private GameMode originalGamemode;
    @Getter
    private User user;
    @Getter
    private ArmorStand stand;
    @Getter
    private ArmorStand msgStand;
    @Getter
    private LivingEntity hat;
    @Getter
    private Craftoblo plugin;

    public Camera(Craftoblo plugin, Player p) {
        this.plugin = plugin;
        this.xOffset = 5;
        this.yOffset = 5;
        this.zOffset = 5;
        this.cachedAngles = new float[]{0, 0};
        this.message = "";
        this.attached = false;
        this.user = DataManager.getUser(p);
        this.stand = null;
        this.msgStand = null;
        this.hat = null;
        this.title = p.getDisplayName();
    }

    public void setOffset(double x, double y, double z) {
        this.xOffset = x;
        this.yOffset = y;
        this.zOffset = z;
    }

    public Player getPlayer() {
        return user.getPlayer();
    }

    public void setMessage(String msg) {
        if (msg == null) {
            this.message = null;
            if (this.msgStand != null) {
                this.msgStand.setCustomName("");
                this.msgStand.setCustomNameVisible(false);
            }
            return;
        }

        this.message = msg.trim();

        if (this.msgStand != null) {
            this.msgStand.setCustomName(this.message);
            this.msgStand.setCustomNameVisible(!this.message.equals(""));
        }
    }

    public void setTitle(String title) {
        if (title == null) {
            this.title = null;
            if (this.stand != null) {
                this.stand.setCustomName("");
                this.stand.setCustomNameVisible(false);
            }
            return;
        }

        this.title = title.trim();

        if (this.stand != null) {
            this.stand.setCustomName(title);
            this.stand.setCustomNameVisible(!this.title.equals(""));
        }
    }

    public void moveStand(Location loc) {
        if (!this.attached) {
            return;
        }

        this.stand.teleport(loc);
        this.hat.teleport(loc.clone().add(0, 1, 0));
        this.msgStand.teleport(loc);
    }

    public void detach() {
        if (!this.attached) {
            return;
        }
        Player p = getPlayer();

        this.attached = false;

        PlayerMoveEvent.getHandlerList().unregister(this);
        EntityDamageByEntityEvent.getHandlerList().unregister(this);
        EntityTargetLivingEntityEvent.getHandlerList().unregister(this);

        Craftoblo.gi().getAnimationManager().stopArmorStandAnimations(stand);
        SetupScorboard.bossBars.get(p.getUniqueId()).setVisible(false);
        SetupScorboard.bossBars.get(p.getUniqueId()).removeAll();

        p.setAllowFlight(false);
        p.setInvulnerable(false);
        p.setFlying(false);
        p.setGameMode(originalGamemode);
        p.teleport(stand);
        p.removePotionEffect(PotionEffectType.INVISIBILITY);

        stand.remove();
        hat.remove();
        msgStand.remove();

        stand = null;
        hat = null;
        msgStand = null;
    }

    public void attach() {
        Player p = getPlayer();

        Location loc = p.getLocation().clone();

        if (this.stand == null || this.stand.isDead())
            this.stand = (ArmorStand) p.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);

        if (this.msgStand == null || this.msgStand.isDead())
            this.msgStand = (ArmorStand) p.getWorld().spawnEntity(loc, EntityType.ARMOR_STAND);

        if (this.hat == null || this.msgStand.isDead())
            this.hat = (LivingEntity) p.getWorld().spawnEntity(loc, EntityType.PIG);

        this.originalGamemode = p.getGameMode();
        this.attached = true;

        Location ploc = stand.getLocation().add(this.xOffset, this.yOffset, this.zOffset);
        p.teleport(ploc);

        this.cachedAngles = CameraMath.faceLocation(p, this.stand.getEyeLocation());
        ploc.setYaw(cachedAngles[0]);
        ploc.setPitch(cachedAngles[1]);
        ploc.add(0, 0.00001, 0);

        p.teleport(ploc);
        p.setGameMode(GameMode.SURVIVAL);
        p.setAllowFlight(true);
        p.setFlying(true);
        p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 9999 * 20, 1, true, false));
        p.setInvulnerable(true);

        this.stand.setBasePlate(false);
        this.stand.setArms(true);
        this.stand.setAI(false);
        this.stand.setCollidable(false);
        this.stand.setInvulnerable(true);
        this.stand.setGravity(false);
        if (this.title != null) {
            this.stand.setCustomName(this.title);
            this.stand.setCustomNameVisible(!this.title.equals(""));
        }

        this.msgStand.setVisible(false);
        this.msgStand.setAI(false);
        this.msgStand.setCollidable(false);
        this.msgStand.setInvulnerable(true);
        this.msgStand.setGravity(false);
        if (this.message != null) {
            this.msgStand.setCustomName(this.message);
            this.msgStand.setCustomNameVisible(!this.message.equals(""));
        }

        this.hat.setAI(false);
        // this.hat.setInvulnerable(true);
        this.hat.setGravity(false);
        this.hat.setCustomName("HEART_" + p.getUniqueId().toString());
        this.hat.setCustomNameVisible(false);
        this.hat.teleport(ploc.clone().add(0, 1, 0));
        this.hat.setSilent(true);
        this.hat.setMetadata("HEART", new FixedMetadataValue(this.plugin, p.getUniqueId().toString()));
        hat.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 9999 * 20, 1, true, false));
        hat.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 9999 * 20, 20, true, false));

        ItemStack skull = new ItemStack(Material.SKULL_ITEM);
        skull.setDurability((short) 3);
        SkullMeta sm = (SkullMeta) skull.getItemMeta();
        sm.setOwner(p.getName());
        skull.setItemMeta(sm);
        stand.setHelmet(skull);

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(PlayerMoveEvent e) {
        if (e.getPlayer() != getPlayer()) {
            return;
        }

        Location from = e.getFrom().clone();
        Location to = e.getTo().clone();

        Player p = getPlayer();
        if (!CameraManager.getManager().isAttatched(p))
            return;
        e.setCancelled(false);
        Direction asYaw = CameraMath.getMovingDirection(e.getFrom().clone(), e.getTo().clone());
        stand.getLocation().setYaw(asYaw.toDegree());

        // Player
        Location ploc = e.getTo().clone();
        Location sloc = p.getLocation().clone();
        float[] rotations = cachedAngles;

        if (to.getYaw() != rotations[0] || to.getPitch() != rotations[1]) {
            user.sendError("There is no need to move your camera!");
        }

        ploc.setYaw(rotations[0]);
        ploc.setPitch(rotations[1]);
        sloc.setYaw(asYaw.toDegree());
        sloc.setPitch(0);

        e.setTo(ploc.clone());
        Material toType = e.getTo().clone().subtract(5, 4.9, 5).getBlock().getType();
        if (!toType.equals(Material.AIR)) {
            if (!CameraManager.allowedBlocksList.contains(toType)) {
                e.setCancelled(true);
                return;
            }
        }
        sloc.subtract(5, 5, 5);
        moveStand(sloc);
        moveStand(sloc);

        Variables.moving.put(p.getUniqueId(), Variables.movingTimeout);
        if (!Craftoblo.gi().getAnimationManager().isArmorStandPlayingAnimation(stand, CameraManager.walkAnimation))
            Craftoblo.gi().getAnimationManager().playAnimation(stand, CameraManager.walkAnimation, -1);

        if (CameraManager.debug) {
            double FX = from.getX();
            double TX = to.getX();
            double DX = FX - TX;
            double FZ = from.getZ();
            double TZ = to.getZ();
            double DZ = FZ - TZ;
            ActionBar.sendAction(p,
                    asYaw.name() + " (Set To: " + asYaw + ") (Is Set: " + stand.getLocation().getYaw() + ")", false);
            p.sendMessage("X = " + DX + "\nZ = " + DZ);
        }

    }

    @EventHandler
    public void onEnemyHitHeart(EntityDamageByEntityEvent e) {
        if (e.getEntity() != hat)
            return;
        Player p = getPlayer();
        // p.sendMessage("You were hit by a " +
        // e.getDamager().getType().toString());
        p.setInvulnerable(false);
        p.damage(1);
        p.setHealth(Math.min(p.getHealth() + 1, 20.0));
        p.setInvulnerable(true);

    }

    @EventHandler
    public void onEnemyTarget(EntityTargetLivingEntityEvent e) {
        if (e.getTarget() != getPlayer()) {
            return;
        }

        e.setTarget(hat);
    }
}
