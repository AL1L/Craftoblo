package com.al1l.craftoblo.menus;

import com.al1l.craftoblo.abilitys.AbilityObject;
import com.al1l.craftoblo.camera.CameraManager;
import com.al1l.craftoblo.classes.ClassType;
import com.al1l.bukkit.gui.elements.Button;
import com.al1l.bukkit.gui.ChestGui;
import com.al1l.craftoblo.playerdata.DataManager;
import com.al1l.craftoblo.playerdata.User;
import com.al1l.craftoblo.scoreboard.SetupScorboard;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;

public class ClassSelection extends ChestGui {

    public ClassSelection() {
        this.setTitle(ChatColor.RED + "" + ChatColor.UNDERLINE + "Class Selector");
        this.setRows(1);

        int count = 1;
        for (ClassType classType : ClassType.getRealClasses()) {
            this.add((new Button(classType.getIcon()) {
                @Override
                public void onClick(InventoryClickEvent e) {
                    Player player = (Player) e.getWhoClicked();
                    User user = DataManager.getUser(player);
                    classType.onSelect(player);
                    player.sendMessage(ChatColor.AQUA + "Your class was set to " + classType.getFullName(ChatColor.BOLD) + ChatColor.AQUA + ".");
                    user.setCurrentClass(classType);
                    PlayerInventory pinv = player.getInventory();
                    pinv.clear();
                    List<AbilityObject> abilityObject = user.getEquippedAbilities();
                    for (int i = 0; i < abilityObject.size(); i++)
                        user.getPlayer().getInventory().setItem(i, abilityObject.get(i).getItem(user.getPlayer()));
                    player.closeInventory();
                    SetupScorboard.set(player);
                    CameraManager.getManager().detach(player);
                    CameraManager.getManager().attach(player, 5, 5, 5);
                }
            }).setPosistion(count, 0));
            count++;
        }


    }
}
